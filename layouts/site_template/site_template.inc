<?php
/**
 * @file
 * Definition of Lucid site template layout.
 */

$plugin = array(
  'title' => t('Site template'),
  'theme' => 'site-template',
  'icon' => 'site-template.png',
  'category' => 'Lucid',
  'regions' => array(
    'branding' => t('Branding'),
    'branding_left' => t('Branding left'),
    'content' => t('Content'),
    'closure' => t('Closure'),
    'nav' => t('Navigation'),
  ),
);
