<?php
/**
 * @file
 * This layout is intended to be used inside the page content pane. Thats why
 * there is not wrapper div by default.
 */
?>

<div id="page">
  <header class="branding" role="banner">
    <div class="container-12 clearfix">
      <?php if (!empty($content['branding'])): ?>
        <div class="grid-9 alpha">
          <?php print render($content['branding']); ?>
        </div>
        <?php endif; ?>
        <?php if (!empty($content['branding_left'])): ?>
        <aside class="grid-3 omega branding-left">
          <?php print render($content['branding_left']); ?>
        </aside>
        <?php endif; ?>
        <?php if (!empty($content['nav'])): ?>
         <div class="grid-12 alpha omega clearfix" role="navigation">
            <?php print render($content['nav']); ?>
        </div>
      <?php endif; ?>
    </div>
  </header>
  <?php if (!empty($content['content'])): ?>
    <div id="main" role="main">
      <div class="container-12 clearfix">
        <div class="grid-12 clearfix page-content">
          <?php print render($content['content']); ?>
        </div>
      </div>
    </div>
  <?php endif; ?>

  <?php if (!empty($content['closure'])): ?>
    <footer class="closure" role="footer">
      <div class="container-12 clearfix">
        <div class="grid-12 clearfix">
          <?php print render($content['closure']); ?>
        </div>
      </div>
    </footer>
  <?php endif; ?>
</div>
