/**
 * @file
 * JS for attaching a cycle using the JQuery cycle plugin, if available.
 */
(function($) {
  Drupal.lucid = Drupal.lucid || {};

  Drupal.behaviors.lucidCycle = {
    attach: function (context, settings) {
      $(window).load(function () {
        for (var i in settings.lucid.cycle) {
          if (settings.lucid.cycle.hasOwnProperty(i)) {
            var cycle_selector = settings.lucid.cycle[i].selector;
            var cycle_settings = settings.lucid.cycle[i].settings;
            // Use jQuery cycle if available.
            if ($.cycle) {
              var cycle = {
                fx: 'fade',
                speed: 'fast',
                timeout: cycle_settings.interval,
              };
              if (cycle_settings.controls) {
                cycle.next = '.next';
                cycle.prev = '.prev';
              }
              if (cycle_settings.pager) {
                cycle.pager = '.pager';
              }
              $(cycle_selector).cycle(cycle);
            }
            // Provide an extremely basic cycle otherwise.
            else {
              new Cycle($(cycle_selector), cycle_settings);
            }
          }
        }
      });
    }
  }

  /**
   * Construct a cycle of elements.
   */
  function Cycle(element, settings) {
    this.element = element;
    this.settings = settings;
    this.effects = {
      'none': ['hide', 'show'],
      'fade': ['fadeOut', 'fadeIn']
    }
    if (!this.settings.effect) {
      this.settings.effect = 'fade';
    }
    if (typeof this.settings.speed === "undefined") {
      this.settings.speed = 'slow';
    }
    if (!isNaN(parseFloat(this.settings.speed)) && isFinite(this.settings.speed)) {
      this.settings.speed = parseInt(this.settings.speed);
    }
    this.currentIndex = 0;
    var cycle = this;
    var list = $(settings.item_element, element);
    // Bail if we don't have enough items.
    if (list.length < 2) {
      return;
    }
    this.list = $(settings.item_element, element).hide();
    this.current = this.list.first(settings.item_element).show();
    if (settings.controls) {
      this.attachControls();
    }
    if (settings.pager) {
      this.attachPager();
    }
    this.hovering = false;
    // Resizing.
    $(element).css('height', this.current.height() + 'px');
    $(window).resize(function() {
      $(element).css('height', cycle.current.height() + 'px');
    });
    $(element).hover(function () {
      cycle.hovering = true;
    }, function () {
      cycle.hovering = false;
    });
    this.interval = function () {
      if (!cycle.hovering) {
        cycle.next();
      }
    }
    if (parseInt(this.settings.interval)) {
      setInterval(this.interval, parseInt(this.settings.interval));
    }
  }

  Cycle.prototype.changeItem = function (index) {
    var cycle = this;
    var element = $(this.current);
    element[this.effects[this.settings.effect][0]](this.settings.speed, function() {
      cycle.current = cycle.list[index];
      var currentElement = $(cycle.current);
      if (cycle.pagerAttached) {
        $('.pager .page.active', cycle.element).removeClass('active');
        $('.pager .page-' + (index + 1), cycle.element).addClass('active');
      }
      currentElement[cycle.effects[cycle.settings.effect][1]](cycle.settings.speed)
    });
  }

  /**
   * Attach controls to the slideshow.
   */
  Cycle.prototype.attachControls = function () {
    var cycle = this;
    $(this.element).append('<div class="controls"><a class="prev" href="#">&lsaquo;</a><a class="next" href="#">&rsaquo;</a></div>');
    $('.next', this.element).click(function() {
      cycle.next();
      return false;
    });
    $('.prev', this.element).click(function() {
      cycle.prev();
      return false;
    });
  }

  /**
   * Attach a pager to the slideshow.
   */
  Cycle.prototype.attachPager = function () {
    var pager = $('<div>').addClass('pager');
    var cycle = this;
    $(this.list).each(function (i) {
      var page_element = $('<a>').addClass('page').addClass('page-' + (i + 1)).attr('href', '#').html(i + 1);
      if (!i) {
        page_element.addClass('active');
      }
      pager.append(page_element);
    });
    $(this.element).append(pager);
    $('.page', this.element).each(function(i, page) {
      $(page).click(function () {
        cycle.changeItem(i);
        return false;
      });
    });
    this.pagerAttached = true;
  }

  /**
   * Move to the next element in the cycle.
   */
  Cycle.prototype.next = function () {
    this.currentIndex++;
    if (this.currentIndex >= this.list.length) {
      this.currentIndex = 0;
    }
    this.changeItem(this.currentIndex);
  }

  /**
   * Move to the previous element in the cycle.
   */
  Cycle.prototype.prev = function () {
    this.currentIndex--;
    if (this.currentIndex < 0) {
      this.currentIndex = this.list.length-1;
    }
    this.changeItem(this.currentIndex);
  }
  // Append the cycle class to the global lucid namespace.
  Drupal.lucid.Cycle = Cycle;
})(jQuery);
