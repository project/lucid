<?php
/**
 * @file
 * Lucid highglighted style.
 */

/**
 * Plugin definition.
 */
$plugin = array(
  'title' => t('Highlighted'),
  'description' => t('Display the text of this content in a highlighted way.'),
  'render pane' => 'lucid_highlighted_render_pane',
);

/**
 * Theme function for the pane style.
 */
function theme_lucid_highlighted_render_pane($vars) {
  lucid_append_panels_class($vars['pane'], 'highlighted');
  return theme('panels_pane', $vars);
}
