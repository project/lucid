<?php
/**
 * @file
 * Lucid highglighted style.
 */

/**
 * Plugin definition.
 */
$plugin = array(
  'title' => t('Box'),
  'description' => t('Display the content as a box with a background.'),
  'render pane' => 'lucid_box_render_pane',
  'render region' => 'lucid_box_render_region',
);

/**
 * Theme function for the pane style.
 */
function theme_lucid_box_render_pane($vars) {
  lucid_append_panels_class($vars['pane'], 'box');
  return theme('panels_pane', $vars);
}

/**
 * Theme function for the region style.
 */
function theme_lucid_box_render_region($vars) {
  $output = '';
  foreach ($vars['panes'] as $pane) {
    $output .= $pane;
  }
  return "<div class=\"box\">$output</div>";
}

