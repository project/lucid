<?php
/**
 * @file
 * Lucid promo style.
 */

/**
 * Plugin definition.
 */
$plugin = array(
  'title' => t('Promo'),
  'description' => t('Display the content as a promo.'),
  'render pane' => 'lucid_promo_render_pane',
  'render region' => 'lucid_promo_render_region',
  'settings form' => 'lucid_promo_settings_form',
  'pane settings form' => 'lucid_promo_settings_form',
);


function lucid_promo_settings_form($settings, $display) {
  $form = array();
  $form['link'] = array(
    '#type' => 'checkbox',
    '#title' => t('Link this promo'),
    '#default_value' => $settings['link'],
  );
  $form['settings'] = array(
    '#title' => t('Settings'),
    '#type' => 'fieldset',
    '#tree' => TRUE,
    '#dependency' => array(
      'edit-settings-link' => array(TRUE),
    ),
  );
  $form['settings']['path'] = array(
    '#type' => 'textfield',
    '#title' => t('Link path'),
    '#default_value' => $settings['settings']['path'],
    '#weight' => 1,
  );
  $form['style'] = array(
    '#type' => 'select',
    '#title' => t('Style'),
    '#options' => lucid_styles(),
    '#default_value' => $settings['style'],
  );
  // Link fields are rendered differently depending on formatter,
  // so let's have a special case for them.
  if (module_exists('link')) {
    $link_options = array();
    $link_fields = field_read_fields(array('type' => 'link_field'));
    foreach ($display->context as $context_name => $context) {
      if (strstr($context->plugin, 'entity') !== FALSE) {
        list(,$type) = explode(':', $context->plugin);
        foreach ($link_fields as $field_name => $link_field) {
          $instances = field_read_instances(array('field_name' => $field_name, 'entity_type' => $type));
          foreach ($instances as $instance_name => $instance) {
            $link_options[$context_name . '|' . $instance['field_name']] = $context->identifier . ': ' . $instance['field_name'];
          }
        }
      }
      if (!empty($link_options)) {
        $form['settings']['use_link_field'] = array(
          '#type' => 'checkbox',
          '#title' => t('Use link field'),
          '#default_value' => $settings['settings']['use_link_field'],
        );
        $form['settings']['link_field'] = array(
          '#type' => 'select',
          '#title' => t('Link field'),
          '#default_value' => $settings['settings']['link_field'],
          '#dependency' => array(
            'edit-settings-settings-use-link-field' => array(TRUE),
          ),
          '#options' => $link_options,
        );
        $form['settings']['path']['#dependency']['edit-settings-settings-use-link-field'] = array(FALSE);
      }
    }
  }
  lucid_replacement_patterns($form['settings'], $display);
  return $form;
}

function lucid_styles() {
  $styles = array(
    'standard' => t('Standard'),
    'thumb-floated-left' => t('Thumbnail: Floated left'),
  );
  return variable_get('lucid_styles', $styles);
}


function lucid_replacement_patterns(&$form, $display) {
  if (!empty($display->context)) {
    // Set extended description if both CCK and Token modules are enabled, notifying of unlisted keywords
    if (!module_exists('token')) {
      $description = t('If checked, context keywords will be substituted in this content. More keywords will be available if you install the Token module, see http://drupal.org/project/token.');
    }
    else {
      $description = t('If checked, context keywords will be substituted in this content.');
    }
    $form['contexts'] = array(
      '#title' => t('Substitutions'),
      '#type' => 'fieldset',
      '#weight' => 10,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    $rows = array();
    foreach ($display->context as $context) {
      foreach (ctools_context_get_converters('%' . check_plain($context->keyword) . ':', $context) as $keyword => $title) {
        $rows[] = array(
          check_plain($keyword),
          t('@identifier: @title', array('@title' => $title, '@identifier' => $context->identifier)),
        );
      }
    }
    $header = array(t('Keyword'), t('Value'));
    $form['contexts']['context'] = array('#markup' => theme('table', array('header' => $header, 'rows' => $rows)));
  }
}

function lucid_process_link($vars, $output) {
  if (!empty($vars['settings']['link'])) {
    if (empty($vars['settings']['settings']['use_link_field']) && !empty($vars['settings']['settings']['path'])) {
      $path = ctools_context_keyword_substitute($vars['settings']['settings']['path'], array(), $vars['display']->context);
      $output = l($output, $path, array('html' => TRUE));
    }
    elseif (!empty($vars['settings']['settings']['use_link_field']) && !empty($vars['settings']['settings']['link_field'])) {
      list($context, $field) = explode('|', $vars['settings']['settings']['link_field']);
      if (isset($vars['display']->context[$context])) {
        list(,$type) = explode(':', $vars['display']->context[$context]->plugin);
        $link = field_get_items($type, $vars['display']->context[$context]->data, $field);
        if (isset($link[0]['url'])) {
          $path = $link[0]['url'];
          $output = l($output, $path, array('html' => TRUE));
        }
      }
    }
  }
  return $output;
}

function lucid_promo_get_classes($vars) {
  $classes = array('promo');
  if (isset($vars['settings']['style'])) {
    $classes[] = $vars['settings']['style'];
  }
  return $classes;
}

/**
 * Theme function for the pane style.
 */
function theme_lucid_promo_render_pane($vars) {
  $output = theme('panels_pane', $vars);
  return '<div class="' . implode(' ', lucid_promo_get_classes($vars)) . '">' . lucid_process_link($vars, $output) . '</div>';
}

/**
 * Theme function for the region style.
 */
function theme_lucid_promo_render_region($vars) {
  $output = '';
  foreach ($vars['panes'] as $pane) {
    $output .= lucid_process_link($vars, $pane);
  }
  return '<div class="' . implode(' ', lucid_promo_get_classes($vars)) . '">' . $output . '</div>';
}
