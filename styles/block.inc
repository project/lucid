<?php
/**
 * @file
 * Lucid block style.
 */

/**
 * Plugin definition.
 */
$plugin = array(
  'title' => t('Block'),
  'description' => t('Display this pane as a block that where you can adjust margin.'),
  'render pane' => 'lucid_block_render_pane',
  'render region' => 'lucid_block_render_region',
  'settings form' => 'lucid_block_settings_form',
  'pane settings form' => 'lucid_block_settings_form',
);

/**
 * Settings form.
 */
function lucid_block_settings_form($settings, $display) {
  $form = array();
  $form['margin_top'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add top margin'),
    '#default_value' => $settings['margin_top'],
  );
  $form['margin_bottom'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add bottom margin'),
    '#default_value' => $settings['margin_bottom'],
  );
  return $form;
}

/**
 * Theme function for the pane style.
 */
function theme_lucid_block_render_pane($vars) {
  $settings = $vars['settings'];
  if (!empty($settings['margin_top'])) {
    lucid_append_panels_class($vars['pane'], 'margin-top');
  }
  if (!empty($settings['margin_bottom'])) {
    lucid_append_panels_class($vars['pane'], 'margin-bottom');
  }
  return theme('panels_pane', $vars);
}

/**
 * Theme function for the region style.
 */
function theme_lucid_block_render_region($vars) {
  $output = '';
  foreach ($vars['panes'] as $pane) {
    $output .= $pane;
  }
  return "<div class=\"box\">$output</div>";
}

