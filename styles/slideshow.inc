<?php

$plugin = array(
  'title' => t('Slideshow'),
  'description' => t('Display the content of this pane as a slideshow.'),
  'render region' => 'lucid_slideshow_render_region',
  'render pane' => 'lucid_slideshow_render_pane',
  'settings form' => 'lucid_slideshow_settings_form',
  'pane settings form' => 'lucid_slideshow_pane_settings_form',
);

/**
 * Theme function for the region style.
 */
function theme_lucid_slideshow_render_region($display, $panel_id, $panes, $settings) {
  $output = '';
  foreach ($panes as $pane_id => $pane) {
    $output .= '<div class="item">' . panels_render_pane($pane, $display->content[$pane_id], $display) . '</div>';
  }
  drupal_add_js(drupal_get_path('theme', 'lucid') . '/js/lucid.cycle.js');
  drupal_add_js(array('lucid' => array('slideshow' => $settings)), 'setting');
  return $output;
}

/**
 * Theme function for the pane style.
 */
function theme_lucid_slideshow_render_pane($vars) {
  static $count = 0;
  $id = "cycle-$count";
  $count++;
  drupal_add_js(drupal_get_path('theme', 'lucid') . '/js/lucid.cycle.js');
  drupal_add_js(array('lucid' => array('cycle' => array(array('selector' => "#$id", 'settings' => $vars['settings'])))), 'setting');
  return "<div class=\"cycle\" id=\"$id\">" . theme('panels_pane', $vars) . '</div>';
}

/**
 * Settings form for the slideshow.
 */
function lucid_slideshow_settings_form($settings) {
  $form = array();
  $form['pager'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use a pager'),
    '#default_value' => $settings['pager'],
  );
  $form['controls'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use controls'),
    '#default_value' => $settings['controls'],
    '#description' => t('Display next and previous buttons'),
  );
  $form['effect'] = array(
    '#type' => 'select',
    '#title' => t('Effect'),
    '#default_value' => $settings['effect'],
    '#options' => array('none' => t('No effect'), 'fade' => t('Fade')),
  );
  $form['interval'] = array(
    '#type' => 'textfield',
    '#title' => t('Interval'),
    '#default_value' => $settings['interval'],
    '#description' => t('The interval in milliseconds. Set to 0 for no interval.'),
  );
  $form['speed'] = array(
    '#type' => 'radios',
    '#title' => t('Speed'),
    '#options' => array('fast' => t('Fast'), 'slow' => t('Slow'), 0 => t('Immediate')),
    '#default_value' => $settings['speed'],
  );
  return $form;
}

function lucid_slideshow_pane_settings_form($settings) {
  $form['item_element'] = array(
    '#type' => 'textfield',
    '#default_value' => $settings['item_element'],
    '#title' => t('Item element'),
    '#description' => t('The element to use as an item in the slideshow'),
  );
  return $form + lucid_slideshow_settings_form($settings);
}
