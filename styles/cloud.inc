<?php
/**
 * @file
 * Lucid cloud style.
 */

/**
 * Plugin definition.
 */
$plugin = array(
  'title' => t('Cloud'),
  'description' => t('Display the content as a cloud.'),
  'render pane' => 'lucid_cloud_render_pane',
);

/**
 * Theme function for the pane style.
 */
function theme_lucid_cloud_render_pane($vars) {
  lucid_append_panels_class($vars['pane'], 'cloud');
  return theme('panels_pane', $vars);
}
