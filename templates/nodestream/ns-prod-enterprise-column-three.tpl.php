<?php

/**
 * @file
 * This layout is intended to be used inside the page content pane. Thats why
 * there is not wrapper div by default.
 */
?>

<?php if (!empty($content['header'])): ?>
  <div class="header grid-12">
      <?php print render($content['header']); ?>
  </div>
<?php endif; ?>
<?php if (!empty($content['col1'])): ?>
  <div class="col-1 grid-4 col first">
    <?php print render($content['col1']); ?>
  </div>
<?php endif; ?>
<?php if (!empty($content['col2'])): ?>
  <div class="col-2 grid-4 col">
    <?php print render($content['col2']); ?>
  </div>
<?php endif; ?>

<?php if (!empty($content['col3'])): ?>
  <div class="col-3 grid-4 col last">
    <?php print render($content['col3']); ?>
  </div>
<?php endif; ?>
<?php if (!empty($content['footer'])): ?>
  <div class="footer grid-12">
      <?php print render($content['footer']); ?>
  </div>
<?php endif; ?>
