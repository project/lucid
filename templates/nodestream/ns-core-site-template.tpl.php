<?php
/**
 * @file
 * Override of the site template layout provided by ns_core
 */
?>

<div id="page">
  <header class="branding" role="banner">
    <div class="container-12 clearfix">
      <?php if (!empty($content['branding'])): ?>
        <div class="grid-12 clearfix">
          <?php print render($content['branding']); ?>
        </div>
      <?php endif; ?>
      <?php if (!empty($content['branding_left'])): ?>
        <div class="grid-6 branding-left">
          <?php print render($content['branding_left']); ?>
        </div>
      <?php endif; ?>
      <?php if (!empty($content['branding_right'])): ?>
        <div class="grid-6 branding-right">
          <?php print render($content['branding_right']); ?>
        </div>
      <?php endif; ?>
    </div>
    <?php if (!empty($content['nav'])): ?>
      <div id="navigation" class="container-12 clearfix" role="navigation">
          <div class="grid-12">
            <?php print render($content['nav']); ?>
          </div>
      </div>
    <?php endif; ?>
  </header>
  <?php if (!empty($content['main'])): ?>
    <div id="main" class="container-12 clearfix page-content" role="main">
      <?php print render($content['main']); ?>
    </div>
  <?php endif; ?>
</div>
<?php if (!empty($content['footer'])): ?>
  <footer id="footer" role="footer">
    <div class="container-12">
      <div class="grid-12 alpha omega clearfix">
        <?php print render($content['footer']); ?>
      </div>
   </div>
  </footer>
<?php endif; ?>
