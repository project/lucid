<?php
/**
 * @file
 * This page.tpl is the one that is used when panels everywhere
 * is disabled.
 */
?>

<div id="page">
  <?php if (!$in_overlay): ?>
    <header class="branding" role="banner">
      <div class="<?php print $container_class ?> clearfix">
        <div class="<?php print $grid_class ?> clearfix">
          <?php if ($site_name || $site_slogan || $logo): ?>
            <hgroup>
              <?php if ($logo): ?>
                <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
                  <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
                </a>
              <?php endif; ?>
              <h1 id="site-name">
                <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><?php print $site_name; ?></a>
              </h1>
              <?php if ($site_slogan): ?>
                <div id="site-slogan"><?php print $site_slogan; ?></div>
              <?php endif; ?>
            </hgroup>
          <?php endif; ?>
          <?php print render($page['header']); ?>
        </div>
      </div>
      <?php if ($main_menu): ?>
        <div id="navigation" role="navigation">
          <div class="<?php print $container_class ?> clearfix">
            <div class="<?php print $grid_class ?> clearfix">
              <nav class="main-menu">
                <?php print theme('links__system_main_menu', array('links' => $main_menu, 'attributes' => array('id' => 'main-menu', 'class' => array('links', 'inline', 'clearfix')))); ?>
              </nav>
        </div> <!-- /#main-menu -->
      <?php endif; ?>
    </header>
  <?php endif; ?>
  <div id="main" role="main">
    <div class="<?php print $container_class ?> clearfix">
      <div class="<?php print $grid_class ?> clearfix">
        <div class="page-content">
        <?php if ($messages): ?>
          <div id="console" class="clearfix"><?php print $messages; ?></div>
        <?php endif; ?>
        <?php if (!$in_overlay): ?>
          <?php print render($title_prefix); ?>
          <?php if ($title): ?><h1 class="title" id="page-title"><?php print $title; ?></h1><?php endif; ?>
          <?php print render($title_suffix); ?>
        <?php endif; ?>
        <?php if ($tabs): ?>
          <div id="tabs">
            <?php print render($tabs); ?>
          </div>
        <?php endif; ?>
        <div class="page-content clearfix">
          <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
          <?php print render($page['content']); ?>
          <?php print render($content['main']); ?>
        </div>
        </div>
      </div>
    </div>
  </div>
  <?php if (!empty($content['footer'])): ?>
    <footer id="footer">
      <div class="<?php print $container_class ?> clearfix">
        <div class="<?php print $grid_class ?> clearfix">
          <?php print $feed_icons; ?>
        </div>
      </div>
    </footer>
  <?php endif; ?>
</div>
